# POSGAMP_2022

In der Mitschrift werden wichtige Unterrichtskonzepte notiert, ebenso währende der Bearbeitung der Arbeitsaufträge auftauchende Fragen.


## 20.9.2022: Einleitung PHP  - Teil 1
Kurze Einleitung in die Konzepte von PHP als Server-seitige Skriptsprache auf der Command Line und als Interpreter für eingebetteten Code in HTML-Dokumenten. 

![Überblick](https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/PHP_funktionsweise.svg/780px-PHP_funktionsweise.svg.png?20121129070241)

Tags: HTTP-Request-Response, dynamische Inhalte mit serverseitiger Programmierung

Besprochene Konzepte:
- Schwache, dynamische Typisierung von PHP. Optionale Typangaben.
- Variablen (Definition, Wertzuweisung, Literalschreibweisen, Variablensubstitution)
- Fertige Funktionen verwenden (Doku lesen, call-by-value vs. call-by-reference)
- Eigene Funktionen definieren und aufrufen
- Einfache Klassen definieren und verwenden
- Standard-konforme Dokumentation schreiben
- Indizierte Arrays verwenden

````php
//Definieren von Variablen
$name = 'Michael';
echo "Username $name";

//Definition Mit Datentyp
String $nachname = 'Gamper';

/**
 * Diese Funktion bildet aus einem Vornamen und einem Nachnamen einen String
 * @param String Vorname
 * @param String Nachname
 * @param String Anrede Optionaler Parameter. Wenn nicht angegeben, ist die Anrede 'Herr'
 * @return String Fullname
 */
function getFullname($vname, $nname, $anrede='Herr')
{
    //TODO: Parameter validieren
    
    return $anrede .' ' . $vname.' '.$nname;
}

//Aufruf der Funktion
getFullname('Michael','Gamper')

````
## 27.09.2022: Einleitung PHP Teil 2

Besprochene Konzepte:

-Formularverarbeitung   
-Datenübermittllung
-HTTP GET und POST       :     form method="GET" oder "POST"
-Validierung am Client   :    type="email" z.B für eine Clientseitige Validierung durch HTML/JS (checkt emailadresse) JS
-Validierung am Server   :     PHP

### Formularverarbeitung   
```` php
                     <form action="register.php" method="GET">     </form>                                                 
                     <input name="studiname" type="text"   required="" placeholder="Maxine Musterfrau" />      
					 <input name="submit"  value="Absenden"       />                                           
                     <label for="studiname"> Name Studierende/r   </label>        
````

Formulare sind in erster Instanz ein HTML-Element, erst wenn das Formular ausgewertet werden soll, vermischen sich diese mit PHP.
Mit *action:* legt man die Datei bzw dessen Pfad fest, die nach der "Aktion" aufgerufen werden soll.
Durch *method:* wird bestimmt ob die Methode **get** oder post** verwendet wird, um weiter mit den Daten zu arbeiten.

### **HTTP GET und POST**

In PHP können Variablen von einer Seite zu einer anderen übertragen werden. Für diesen Anwendungsfall gibt es zwei Methoden, die sich in der Art der Übertragung an die nächste Seite unterscheiden. Die beiden Methoden sind **GET** und **POST**.

#### **GET: Überträgt die Variablen sichtbar in Adresszeile (URL), limitierte Anzahl / Länge an Variablen**

#### **POST: Überträgt die Variablen für den Client unsichtbar im Hintergrund, unlimitierte Anzahl / Länge an Variablen**


### **Validierung am Client**  
````html
 type="email" 
````
z.B für eine Clientseitige Validierung durch HTML/JS (checkt emailadresse) JS

### **Validierung am Server**   
Hier kommt nun die Vorhin im Formular angegebene Datei "action="php/register.php"" zum Einsatz. Sendet der Benutzer sein ausgefülltes Formular ab, wird diese Datei aufgerufen. Es kann nun, wie in folgendem Beispiel, überprüft werden, ob der mitgegebene Name den Voraussetzungen, die der Programmierer gesetzt hat, entspricht:
````php
if(isset($_POST['studiname'])){
        $studiname = $_POST['studiname'];
        if(checkName($studiname) == true){
            echo 'Validierung hat funktioniert';
            //Maskieren gefährlicher Zeichen
            echo htmlspecialchars($studiname);
        } else {
            echo 'Validierung hat nicht funktioniert';
        }
    }
function checkName($name) {
        $regex = '/\D{3,255}/m';
        if(preg_match($regex, $name) === 1) {
            return true;
        }
        return true;
    }
````

````var_dump(variable)````: "force echo" ---> mit details für entwickler, echo nur string 
                 
````if (isset($_POST['studiname'])) ````   --> wenn 'studiname' gesetzt ist, führe .... aus

````if(preg_match($regex,$name) ===  1 ```` --> wird ausgeführt wenn und nur wenn Integer 1 returned wird.   

## 04.10.2022: Einleitung PHP - Notenerfassung 2.0

---
1. Formular erstellen
2. Eingabefelder definieren
3. Clientseitige Validierung
4. Serverseitige Validierung
5. Ausgabe der Ergebnisse
---

### **1. Formular erstellen**
Das HTML-Formular soll in die Starseite index.php integriert werden. Die Übermittlung der Daten soll mittels http Post im selben Skript erfolgen (action & method-Parameter).

Das HTML ````<form>````-Element wird verwendet, um ein HTML-Formular für Benutzereingaben zu erstellen:
````html
<form>
.
form elements
.
</form>
````
Das ````<form>```` Element ist ein Container für verschiedene Arten von Eingabeelementen, wie z. B.: Textfelder, Kontrollkästchen, Optionsfelder, Senden-Schaltflächen usw.

````html
<form id="form_grade" action="index.php" method="post">
````

Das *method* Attribut gibt an, wie Formulardaten gesendet werden (die Formulardaten werden an die im *action* Attribut angegebene Seite gesendet).

Die Formulardaten können als URL-Variablen (mit method="get") oder als HTTP-Posttransaktion (mit method="post") gesendet werden.


### **2. Eingabefelder definieren**
Für jedes Eingabefeld muss ein eigenes HTML-Input-Element erstellt werden. Für die spätere Datenverarbeitung ist es wichtig, dass jedes Element einen eindeutigen Parameter *name* besitzt. Zur Formularübermittlung wird ein Submit-Button benötigt und zum Leeren des Formulares eine Reset-Button, Link auf index.php oder JavaScript.

Das HTML- ````<input>```` Element ist das am häufigsten verwendete Formularelement.

Ein ````<input>```` Element kann je nach type Attribut auf viele Arten dargestellt werden.

````html
                <input type="text"
                       name="name"
                       class="form-control <?= isset($errors['name']) ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars($name) ?>"
                       maxlength="20"
                       required="required"
                />
 ````


### **3. Clientseitige Validierung**
Die clientseitige Validierung verhindert fehlerhafte Benutzereingaben und ermöglicht eine sofortige Rückmeldung während der Eingabe.
- Name: type=text, maxlength=25, required
- Prüfungsdatum: type=date, required
- Fach: Select-Element, required, option für jedes Fach, value wird abgekürzt, z.B. „m“
- Note: type=number, min=1, max=5, required

Das Datum überprüfen wir mit JavaScript, indem wir eine dafür entsprechende Funktion erstellen. Das JavaScript-file muss im index.php eingebunden werden.
Jede Benutzereingabe sollte überprüft werden, damit z.B. bei *value* kein schadhafter Code eingeschleußt werden kann. In unserem Fall werden mit der Funktion ````value="<?= htmlspecialchars($name) ?>"```` alle Sonderzeichen wie z.B. <, >, & etc. in die jeweiligen HTML-Codierungen umgewandelt.



### **4. Serverseitige Validierung**
Die serverseitige Validierung verhindert die Weiterverarbeitung und Speicherung fehlerhafter Daten.
- Die Formulardaten werden aus dem $_POST-Array im index.php-Skript entnommen
- Die weitere Validierung und Verarbeitung erfolgt mittels Funktionen in einem eigenen PHP-File
- Neben einer Gesamt-Validierungsfunktion muss für jedes Eingabefeld eine eigene Validierungsfunktion erstell werden (Rückgabetyp boolean)
- Alle Validierungsfehler werden in einem assoziativen Fehlerarray gespeichert

Es wird im HTML-Code vor dem ````<form>````-Teil PHP-Code eingebaut. Dieser wird zur Validierung verwendet und behält die eingegebenen
Daten nach Absenden in den Datenfeldern.

````php
<?php

require "lib/func.inc.php";

$name = ""; 
$email = "";
$examDate = "";
$grade = "";
$subject = "";

````
Wenn die Variablen nicht vorher initialisiert wurden, erscheint eine Fehlermeldung im Input-Feld.

### **5. Ausgabe der Daten**
Bei Validierungsfehler soll im Formular jeweils bei dem fehlerhaften Eingabefeld eine sinnvolle Fehlermeldung ausgegeben werden.


## 18.10.2022: Reflexion zu Lernaufgabe 1.1 und zur Theorie ##

Was habe ich im Rahmen des Kurses gelernt? PHP, Server-,Clientseitige Validierungen, Formularverarbeitung
Vorher hatte ich wenig Ahnung. Aber jetzt weiß ich, dass es nie nur eine clientseitige Validierung geben darf.
Mir war manches rätselhaft. Aber jetzt verstehe ich den Unterschied zu HTTP GET und POST.
Was ich dazugelernt habe und jetzt besser kann: PHP
Spannend und interessant war für mich vor allem der Umgang mit dem BrowserDebugger.
Schwer gefallen ist mir die Syntax von PHP (Warum?) Weil sie doof ist.
Was war mein größtes Aha-Erlebnis? 
Was habe ich noch überhaupt nicht verstanden? Das GRID-System, obwohl ich es schon von letztem Semester beherrschen sollte
Welche Fragen sind für mich noch offen geblieben? Wie ich in einem PHP echo eine Funktion einbauen kann.
Welche der Lernziele habe ich bereits erreicht? Lernziele Teil 1 - Formularverarbeitung / Datenvalidierung
Welche der Expertenstatements kann ich bereits interpretieren und wie sieht meine Interpretation dazu aus?
Wie wird es weitergehen? Ich schätze leider nicht einfacher und weiters sehr PHP-lastig.
... d.h. was möchtest du als nächstes lernen, was muss du nochmals wiederholen, wie kannst du deinen Lernfortschritt weiter verbessern? Indem ich (eventuell nicht 30 Minuten vor Abgabeende) fleißig die Aufgaben mache!
